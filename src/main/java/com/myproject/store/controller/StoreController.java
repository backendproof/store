package com.myproject.store.controller;

import com.myproject.store.model.Store;
import com.myproject.store.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(path = "/store")
public class StoreController {

    @Autowired
    private StoreRepository storeRepository;

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<Store> getAllStores() {
        return storeRepository.findAll();
    }

    @GetMapping(path = "/add")
    public @ResponseBody String addNewStore (@RequestParam String code,
                                                @RequestParam String address,
                                                @RequestParam String cellphone) {
        Store store = new Store();
        store.setCode(code);
        store.setAddress(address);
        store.setCellphone(Long.parseLong(cellphone));

        storeRepository.save(store);

        return "Saved";
    }

    @GetMapping(path = "/remove")
    public @ResponseBody String removeStore(@RequestParam int id) {
        storeRepository.delete(id);
        return "Removed";
    }
}
