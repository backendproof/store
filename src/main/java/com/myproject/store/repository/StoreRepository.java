package com.myproject.store.repository;

import com.myproject.store.model.Store;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<Store, Integer>{
}
